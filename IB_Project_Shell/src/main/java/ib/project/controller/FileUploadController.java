package ib.project.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping("auth/upload")
@CrossOrigin("*")
public class FileUploadController {

	private static String UPLOAD_FOLDER = "C:\\Users\\Marko\\Desktop\\UploadWeb\\";

	@PostMapping("/zip")
	@PreAuthorize("hasRole('USER')")
	public void fileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes)
			throws FileNotFoundException {

		if (file.isEmpty()) {
			throw new FileNotFoundException("Please select a file and try again");
		}

		try {
			// read and write the file to the slelected location-
			byte[] bytes = file.getBytes();
			Path path = Paths.get(UPLOAD_FOLDER + file.getOriginalFilename());
			Files.write(path, bytes);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
