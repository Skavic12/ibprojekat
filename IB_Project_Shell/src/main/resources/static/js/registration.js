function register(){
	var username =  $('#username').val().trim();
	var password = $('#password').val().trim();
	console.log(username+" "+password);
	if(username=="" || password==""){
		alert("All fields must be filled.")
		return;
	}
	var data = {
			'username':username,
			'password':password
	}
	console.log(data);
	
	$.ajax({
		type: 'POST',
        contentType: 'application/json',
        url: 'http://localhost:8083/auth/register',
        data: JSON.stringify(data),
        dataType: 'json',
        crossDomain: true,
		cache: false,
		processData: false,
		success:function(response){
			alert("Uspesno ste se registrovali")
		},
		error: function (jqXHR, textStatus, errorThrown) {  
			if(jqXHR.status=="403"){
				alert("Neuspesno");
			}
		}
	});
	
}